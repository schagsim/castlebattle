# CastleBattle

A 2D strategy game inspired by Castle Fight, a Warcraft 3 and Starcraft mode where two sides throw specialized units against each other without the ability to control them.

To run the game, you need to set-up the environment first.
Watch here https://www.youtube.com/watch?v=_9yem5dJt2E&list=PLRtjMdoYXLf7Lk_mJ0WiFNUJZOoDhIynr&index=1

The game is split to front-end (display output and receive input) and back-end.

The player communicates with the PlayerController which receives the input. This PlayerController then communicates with GameController which serves as an API for the game back-end. No game state update can be performed without calling the game controller.

The GameController updates GameData and GameGraphics. It also uses UnitGovernor to take care of units' lifetime.
GameController calls UnitBuilder when a new unit should be created.

GameData holds all information about the game while GameGraphics stores textures and sprites.

The PlayerGraphicController reads data from GameGraphics and renders them.

![Architecture Overview](Documentation/Internal.PNG)

This design should be general enough to be able to add additional races and units without updating the core functionality too much.