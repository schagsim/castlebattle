#ifndef MapNavig
#define MapNavig

#include <SFML/Graphics.hpp>

class MapNavigator
{
public:
	static const sf::Vector2f GetNewPositionFromCurrentPosition(float distanceSupposedToTravel, const sf::Vector2f targetLocation, const sf::Vector2f currentPosition);

	static const float GetDistance(const sf::Vector2f firstPoint, const sf::Vector2f secondPoint);
};

#endif // !MapNavig
