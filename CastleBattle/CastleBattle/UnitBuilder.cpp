#include "UnitBuilder.h"

std::unique_ptr<Unit> UnitBuilder::CreateUnit(
	UnitFaction unitFaction, UnitType unitType,
	sf::Vector2f unitInitialPosition, sf::Vector2f unitTargetPosition,
	int playerNoAllegience,
	const std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>> & unitTextures)
{
	std::unique_ptr<Unit> newUnit = nullptr;

	switch (unitFaction)
	{
	case UnitFaction::Human:
		newUnit = HumanUnitBuilder::CreateNewUnit(unitType, unitInitialPosition, unitTargetPosition, playerNoAllegience, unitTextures);
		break;
	default:
		break;
	}

	return std::move(newUnit);
}

bool UnitBuilder::LoadUnitTextures(std::unique_ptr<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>> & unitTextures)
{
	std::unique_ptr<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>> loadedTextures = std::make_unique<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>>();

	std::unique_ptr<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>> humanTextures = std::make_unique<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>>();
	if (!(HumanUnitBuilder::LoadUnitTextures(humanTextures))) return false;

	for (auto humanTexture = humanTextures->begin(); humanTexture != humanTextures->end(); humanTexture++)
	{
		auto & currentTexture = **humanTexture;
		loadedTextures->insert(std::make_unique<std::pair<UnitType, std::unique_ptr<sf::Texture>>>(currentTexture.first, std::move(currentTexture.second)));
	}

	unitTextures = std::move(loadedTextures);

	return true;
}
