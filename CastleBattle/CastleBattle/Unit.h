#ifndef AUnitH
#define AUnitH

#include <SFML/Graphics.hpp>

#include "UnitEnumTypes.h"

class Unit
{
public:
	Unit() :
		_unitHealth(1), _unitMaxHealth(1),
		_unitAllegiance(0),
		_unitPosition(sf::Vector2f(0, 0)), _unitTargetPosition(sf::Vector2f(0, 0)),
		_unitSprite(nullptr), _unitIconSprite(nullptr),
		_unitSpeedPixelsPerSecond(0),
		_unitFaction(UnitFaction::Neutral), _unitType(UnitType::Unspecified),
		_armorType(ArmorType::None), _damageType(DamageType::None),
		_rangeInPixels(0) 
	{
		_lastMovedElapsed.restart();
	}

	Unit(
		long unitMaxHealth,
		int playerAllegiance,
		sf::Vector2f unitInitialPosition,
		sf::Vector2f unitTargetPosition,
		std::shared_ptr<sf::Sprite> unitSprite,
		std::shared_ptr<sf::Sprite> unitIconSprite,
		float speedPixelsPerSecond,
		UnitFaction unitFaction, UnitType unitType,
		ArmorType armorType, DamageType damageType,
		float unitRange
		) :
		_unitHealth(unitMaxHealth), _unitMaxHealth(unitMaxHealth),
		_unitAllegiance(playerAllegiance),
		_unitPosition(unitInitialPosition), _unitTargetPosition(unitTargetPosition),
		_unitSprite(unitSprite), _unitIconSprite(unitIconSprite),
		_unitSpeedPixelsPerSecond(speedPixelsPerSecond),
		_unitFaction(unitFaction), _unitType(unitType),
		_armorType(armorType), _damageType(damageType),
		_rangeInPixels(unitRange)
	{
		_lastMovedElapsed.restart();
	}

	virtual const long GetUnitHealth() const { return _unitHealth; }
	virtual const long GetUnitMaxHealth() const { return _unitMaxHealth; }

	virtual const int GetUnitPlayerAllegiance() const { return _unitAllegiance; }

	virtual void UpdateUnitPosition(sf::Vector2f newPosition);
	virtual void UpdateUnitTargetPosition(sf::Vector2f newPosition);

	virtual const sf::Vector2f GetUnitPosition() const { return _unitPosition; }
	virtual const sf::Vector2f GetUnitTargetPosition() const { return _unitTargetPosition; }

	virtual const std::shared_ptr<sf::Sprite> GetUnitSprite() const { return _unitSprite; }
	virtual const std::shared_ptr<sf::Sprite> GetUnitIconSprite() const { return _unitIconSprite; }

	virtual const float GetSecondsSinceLastMovedWithReset() { return _lastMovedElapsed.restart().asSeconds(); }

	virtual const float GetUnitSpeedInPixelsPerSecond() const { return _unitSpeedPixelsPerSecond; }

	virtual const UnitFaction GetUnitFaction() const { return _unitFaction; }
	virtual const UnitType GetUnitType() const { return _unitType; }
	virtual const ArmorType GetUnitArmorType() const { return _armorType; }
	virtual const DamageType GetUnitDamageType() const { return _damageType; }
	virtual const float GetUnitRange() const { return _rangeInPixels; }

	virtual ~Unit() {}
private:
	long _unitHealth;
	long _unitMaxHealth;

	int _unitAllegiance;

	std::shared_ptr<sf::Sprite> _unitSprite;
	std::shared_ptr<sf::Sprite> _unitIconSprite;

	sf::Vector2f _unitPosition;
	sf::Vector2f _unitTargetPosition;

	sf::Clock _lastMovedElapsed;

	float _unitSpeedPixelsPerSecond;

	UnitFaction _unitFaction;
	UnitType _unitType;
	ArmorType _armorType;
	DamageType _damageType;
	float _rangeInPixels;
};

#endif // !AUnitH

