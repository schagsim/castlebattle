#include "CastleBattleGame.h"

void CastleBattleGame::Start()
{
	m_renderWindow = InitializeRenderWindow();

	m_gameGraphics = InitializeGameGraphics();
	if (m_gameGraphics == nullptr) return;

	m_controlView = InitializeControlView();

	//Now that we have the control view, it's time to load the UI.
	if (!(m_gameGraphics->LoadGameUI(m_controlView->getSize(), m_controlView->getCenter()))) return;

	m_gameData = InitializeGameData();

	m_gameController = InitializeGameController();

	PlayerController playerController(m_renderWindow, m_controlView, m_gameController);

	PlayerGraphicController playerGraphicController(m_renderWindow, m_controlView, m_gameGraphics);

	UnitGovernor unitGovernor;

	while (m_renderWindow->isOpen())
	{
		playerController.AcceptInput();
		playerController.ProcessActions();

		unitGovernor.Govern(m_gameData->GetUnits());

		//TODO: This might be in a different thread, in PlayerGraphicController class.
		playerGraphicController.Render();
	}
}

RenderWindowPointer CastleBattleGame::InitializeRenderWindow() const
{
	RenderWindowPointer newRenderWindow = std::make_unique<sf::RenderWindow>(sf::VideoMode(sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height), "CastleBattle");
	
	newRenderWindow->setVerticalSyncEnabled(true);
	newRenderWindow->setFramerateLimit(60);

	return std::move(newRenderWindow);
}

ControlViewPointer CastleBattleGame::InitializeControlView() const
{
	if (m_gameGraphics == nullptr) return nullptr;

	ControlViewPointer newControlView = std::make_unique<sf::View>();

	auto gameWorldWidth = m_gameGraphics->GetGameWorldSize().x;
	auto gameWorldHeight = m_gameGraphics->GetGameWorldSize().y;

	newControlView->setCenter(sf::Vector2f(0.5f * gameWorldWidth, 0.5f * gameWorldHeight));
	newControlView->setSize(sf::Vector2f(0.25f * gameWorldWidth, 0.66f * gameWorldHeight));

	return std::move(newControlView);
}

GameGraphicsPointer CastleBattleGame::InitializeGameGraphics() const
{
	GameGraphicsPointer gameGraphics = std::make_unique<GameGraphics>(sf::Vector2f(40, 40));

	if (!gameGraphics->LoadGameGraphics()) return nullptr;

	return std::move(gameGraphics);
}

GameDataPointer CastleBattleGame::InitializeGameData() const
{
	GameDataPointer gameData = std::make_unique<GameData>();

	return std::move(gameData);
}

GameControllerPointer CastleBattleGame::InitializeGameController() const
{
	GameControllerPointer gameController = std::make_unique<GameController>(m_gameData, m_gameGraphics);

	return std::move(gameController);
}
