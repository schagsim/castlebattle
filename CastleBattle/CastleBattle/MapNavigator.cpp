#include "MapNavigator.h"

const sf::Vector2f MapNavigator::GetNewPositionFromCurrentPosition(float distanceSupposedToTravel, const sf::Vector2f targetLocation, const sf::Vector2f currentPosition)
{
	if (GetDistance(targetLocation, currentPosition) <= distanceSupposedToTravel) return targetLocation;

	if (targetLocation.x == currentPosition.x)
	{
		if (std::abs(currentPosition.y + distanceSupposedToTravel - targetLocation.y) < std::abs(currentPosition.y - distanceSupposedToTravel - targetLocation.y))
		{
			return sf::Vector2f(currentPosition.x, currentPosition.y + distanceSupposedToTravel);
		}
		else
		{
			return sf::Vector2f(currentPosition.x, currentPosition.y - distanceSupposedToTravel);
		}
	}

	float slope = (targetLocation.y - currentPosition.y) / (targetLocation.x - currentPosition.x);
	float linearConst = currentPosition.y - slope * currentPosition.x;

	float xdiff = distanceSupposedToTravel * sqrt(1 / (1 + slope * slope));

	float x1 = currentPosition.x + xdiff;
	float x2 = currentPosition.x - xdiff;

	float y1 = slope * x1 + linearConst;
	float y2 = slope * x2 + linearConst;

	if (GetDistance(sf::Vector2f(x1, y1), targetLocation) < GetDistance(sf::Vector2f(x2, y2), targetLocation))
	{
		return sf::Vector2f(x1, y1);
	}
	else
	{
		return sf::Vector2f(x2, y2);
	}
}

const float MapNavigator::GetDistance(const sf::Vector2f firstPoint, const sf::Vector2f secondPoint)
{
	return sqrt((firstPoint.x - secondPoint.x) * (firstPoint.x - secondPoint.x) + (firstPoint.y - secondPoint.y) * (firstPoint.y - secondPoint.y));
}
