#ifndef PlayerControl
#define PlayerControl

#include <memory>

#include <SFML/Graphics.hpp>

#include "GameController.h"

class PlayerController
{
public:
	PlayerController(const RenderWindowPointer & t_renderWindow, const ControlViewPointer & t_controlView, const GameControllerPointer & t_gameController) :
		m_renderWindow(t_renderWindow), m_controlView(t_controlView), m_gameController(t_gameController)
	{
		for (size_t i = 0; i < 9; i++)
		{
			_nextWave.push_back(UnitType::HumanMarauder);
		}
		_nextWaveTimer.restart();
	}

	void AcceptInput() const;

	void ProcessActions();
private:
	const RenderWindowPointer & m_renderWindow;
	const ControlViewPointer & m_controlView;
	const GameControllerPointer & m_gameController;

	std::vector<UnitType> _nextWave;
	sf::Clock _nextWaveTimer;
};

#endif // !PlayerControl
