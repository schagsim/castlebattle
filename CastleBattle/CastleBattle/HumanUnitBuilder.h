#ifndef HumanBuild
#define HumanBuild

#include <memory>

#include "Unit.h"
#include "GameGraphics.h"

class HumanUnitBuilder
{
public:
	static std::unique_ptr<Unit> CreateNewUnit(
		UnitType unitType,
		sf::Vector2f unitInitialPosition, sf::Vector2f unitTargetPosition,
		int playerNoAllegiance,
		const std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>> & unitTextures);

	static bool LoadUnitTextures(std::unique_ptr<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>> & unitOutMap);

private:
	static bool FindTextureInSet(const UnitType unitType, const std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>> & unitTextures, sf::Texture & foundTexture);
};

#endif // !HumanBuild

