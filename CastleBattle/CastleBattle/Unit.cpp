#include "Unit.h"

void Unit::UpdateUnitPosition(sf::Vector2f newPosition)
{
	_unitPosition = newPosition;
	_unitSprite->setPosition(newPosition);
}

void Unit::UpdateUnitTargetPosition(sf::Vector2f newPosition)
{
	_unitTargetPosition = newPosition;
}
