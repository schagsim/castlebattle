#include "UnitGovernor.h"

void UnitGovernor::Govern(const std::vector<std::unique_ptr<Unit>>& _units)
{
	for (auto unit = _units.begin(); unit != _units.end(); unit++)
	{
		auto & currentUnit = **unit;

		//TODO: Find targets

		auto secondsSinceLastMove = currentUnit.GetSecondsSinceLastMovedWithReset();
		auto unitSpeed = currentUnit.GetUnitSpeedInPixelsPerSecond();

		currentUnit.UpdateUnitPosition(MapNavigator::GetNewPositionFromCurrentPosition(
			secondsSinceLastMove * unitSpeed,
			currentUnit.GetUnitTargetPosition(),
			currentUnit.GetUnitPosition()));
	}
}
