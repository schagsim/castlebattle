#include "GameController.h"

void GameController::SpawnNextWave(UnitFaction faction, const std::vector<UnitType> & nextWave, const int playerAllegiance)
{
	auto unit1 = UnitBuilder::CreateUnit(
		faction, nextWave[0],
		sf::Vector2f(108.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 26.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 26.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	auto unit2 = UnitBuilder::CreateUnit(
		faction, nextWave[1],
		sf::Vector2f(108.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 32.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 32.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	auto unit3 = UnitBuilder::CreateUnit(
		faction, nextWave[2],
		sf::Vector2f(108.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 38.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 38.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	auto unit4 = UnitBuilder::CreateUnit(
		faction, nextWave[3],
		sf::Vector2f(114.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 26.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 26.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	auto unit5 = UnitBuilder::CreateUnit(
		faction, nextWave[4],
		sf::Vector2f(114.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 32.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 32.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	auto unit6 = UnitBuilder::CreateUnit(
		faction, nextWave[5],
		sf::Vector2f(114.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 38.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 38.f/ 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	auto unit7 = UnitBuilder::CreateUnit(
		faction, nextWave[6],
		sf::Vector2f(120.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 26.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 26.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	auto unit8 = UnitBuilder::CreateUnit(
		faction, nextWave[7],
		sf::Vector2f(120.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 32.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 32.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	auto unit9 = UnitBuilder::CreateUnit(
		faction, nextWave[8],
		sf::Vector2f(120.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 38.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		sf::Vector2f(20.f / 128.f * m_gameGraphics->GetGameWorldSize().x, 38.f / 64.f * m_gameGraphics->GetGameWorldSize().y),
		playerAllegiance,
		m_gameGraphics->GetUnitTextures());

	m_gameGraphics->AddUnitSprite(unit1->GetUnitSprite());
	m_gameGraphics->AddUnitSprite(unit2->GetUnitSprite());
	m_gameGraphics->AddUnitSprite(unit3->GetUnitSprite());

	m_gameGraphics->AddUnitSprite(unit4->GetUnitSprite());
	m_gameGraphics->AddUnitSprite(unit5->GetUnitSprite());
	m_gameGraphics->AddUnitSprite(unit6->GetUnitSprite());

	m_gameGraphics->AddUnitSprite(unit7->GetUnitSprite());
	m_gameGraphics->AddUnitSprite(unit8->GetUnitSprite());
	m_gameGraphics->AddUnitSprite(unit9->GetUnitSprite());

	m_gameData->AddUnit(std::move(unit1));
	m_gameData->AddUnit(std::move(unit2));
	m_gameData->AddUnit(std::move(unit3));

	m_gameData->AddUnit(std::move(unit4));
	m_gameData->AddUnit(std::move(unit5));
	m_gameData->AddUnit(std::move(unit6));

	m_gameData->AddUnit(std::move(unit7));
	m_gameData->AddUnit(std::move(unit8));
	m_gameData->AddUnit(std::move(unit9));
}
