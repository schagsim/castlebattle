#ifndef GData
#define GData

#include <SFML/Graphics.hpp>

#include "Unit.h"

#define GameDataPointer std::unique_ptr<GameData>

class GameData
{
public:
	void AddUnit(std::unique_ptr<Unit> newUnit);
	void RemoveUnit(std::unique_ptr<Unit> unitToBeRemoved);

	const std::vector<std::unique_ptr<Unit>> & GetUnits() const { return _units; }
private:
	std::vector<std::unique_ptr<Unit>> _units;
};

#endif // !GData
