#ifndef CBGame
#define CBGame

#include "PlayerController.h"
#include "PlayerGraphicController.h"
#include "GameGraphics.h"
#include "GameData.h"
#include "UnitGovernor.h"

///This class is responsible for ownership of all the game objects and references.
///It also initializes the game and all the components of the game.
///This class is a point of multiple thread creation and ownership.
///Serves as a sort of dependency injection. There is, however, no time to implement the actual DI.
class CastleBattleGame
{
public:
	//Game set-up and initialization.
	void Start();

private:
	RenderWindowPointer m_renderWindow;
	RenderWindowPointer InitializeRenderWindow() const;

	ControlViewPointer m_controlView;
	ControlViewPointer InitializeControlView() const;

	GameGraphicsPointer m_gameGraphics;
	GameGraphicsPointer InitializeGameGraphics() const;

	GameDataPointer m_gameData;
	GameDataPointer InitializeGameData() const;

	GameControllerPointer m_gameController;
	GameControllerPointer InitializeGameController() const;
};

#endif // !CBGame
