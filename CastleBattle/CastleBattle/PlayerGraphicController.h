#ifndef PlayerGraphicControl
#define PlayerGraphicControl

#include <SFML/Graphics.hpp>

#include "GameGraphics.h"

class PlayerGraphicController
{
public:
	PlayerGraphicController(const RenderWindowPointer & t_renderWindow, const ControlViewPointer & t_controlView, const GameGraphicsPointer & t_gameGraphics) :
		m_renderWindow(t_renderWindow), m_controlView(t_controlView), m_gameGraphics(t_gameGraphics) {}

	void Render() const;

private:
	const RenderWindowPointer & m_renderWindow;

	const ControlViewPointer & m_controlView;

	const GameGraphicsPointer & m_gameGraphics;
};

#endif // !PlayerGraphicControl
