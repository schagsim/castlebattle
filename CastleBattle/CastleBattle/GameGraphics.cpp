#include "GameGraphics.h"

GameGraphics::GameGraphics()
{
	_unitTextures = std::make_unique<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>>();
	_gameWorldSprites = std::vector<std::unique_ptr<sf::Sprite>>();
	_gameWorldTextures = std::vector<std::unique_ptr<sf::Texture>>();
	m_tileSize = sf::Vector2f(0, 0);
	m_gameWorldDimensions = sf::Vector2f(0, 0);
}

void GameGraphics::SetGameWorldSizeByTile(sf::Vector2f t_newWorldTileSize)
{
	m_tileSize = t_newWorldTileSize;
	m_gameWorldDimensions = sf::Vector2f(
		WorldWidthInTiles * t_newWorldTileSize.x,
		WorldHeightInTiles * t_newWorldTileSize.y);
}

void GameGraphics::SetGameWorldSizeByWorldDimensions(sf::Vector2f t_newWorldDimensions)
{
	m_gameWorldDimensions = t_newWorldDimensions;
	m_tileSize = sf::Vector2f(
		t_newWorldDimensions.x / WorldWidthInTiles,
		t_newWorldDimensions.y / WorldHeightInTiles
	);
}

const bool GameGraphics::LoadGameGraphics()
{
	return LoadGameWorld() && LoadUnitTextures();
}

const bool GameGraphics::LoadGameWorld()
{
	std::unique_ptr<sf::Texture> grassTexture = std::make_unique<sf::Texture>();
	std::unique_ptr<sf::Texture> roadTexture = std::make_unique<sf::Texture>();

	if (!(grassTexture->loadFromFile("../../Resources/Textures/grass-tile.png") &&
		roadTexture->loadFromFile("../../Resources/Textures/road-tile.jpg")))
	{
		return false;
	}

	grassTexture->setRepeated(true);
	roadTexture->setRepeated(true);

	std::unique_ptr<sf::Sprite> upperGrassSprite = std::make_unique<sf::Sprite>();
	std::unique_ptr<sf::Sprite> lowerGrassSprite = std::make_unique<sf::Sprite>();
	std::unique_ptr<sf::Sprite> roadSprite = std::make_unique<sf::Sprite>();

	upperGrassSprite->setTexture(*grassTexture);
	upperGrassSprite->setTextureRect(sf::IntRect(0, 0, (int)m_gameWorldDimensions.x, (int)(m_gameWorldDimensions.y * 5 / 16)));
	upperGrassSprite->setPosition(sf::Vector2f(0, 0));

	lowerGrassSprite->setTexture(*grassTexture);
	lowerGrassSprite->setTextureRect(sf::IntRect(0, 0, (int)m_gameWorldDimensions.x, (int)(m_gameWorldDimensions.y * 5 / 16)));
	lowerGrassSprite->setPosition(sf::Vector2f(0, m_gameWorldDimensions.y * 11 / 16));

	roadSprite->setTexture(*roadTexture);
	roadSprite->setTextureRect(sf::IntRect(0, 0, (int)m_gameWorldDimensions.x,(int)(m_gameWorldDimensions.y * 6 / 16)));
	roadSprite->setPosition(sf::Vector2f(0, m_gameWorldDimensions.y * 5 / 16));

	_gameWorldTextures.push_back(std::move(grassTexture));
	_gameWorldTextures.push_back(std::move(roadTexture));

	_gameWorldSprites.push_back(std::move(upperGrassSprite));
	_gameWorldSprites.push_back(std::move(lowerGrassSprite));
	_gameWorldSprites.push_back(std::move(roadSprite));

	return true;
}

const bool GameGraphics::LoadGameUI(sf::Vector2f controlViewSize, sf::Vector2f controlViewCenter)
{
	_UIBackgroundTexture = std::make_unique<sf::Texture>();

	if (!(_UIBackgroundTexture->loadFromFile("../../Resources/Textures/metal-ui.png")))
	{
		return false;
	}

	_UIBackgroundTexture->setRepeated(true);

	_UIBackgroundSprite = std::make_unique<sf::Sprite>();

	_UIBackgroundSprite->setTexture(*_UIBackgroundTexture);
	_UIBackgroundSprite->setTextureRect(sf::IntRect(0, 0, (int)(controlViewSize.x / 6.f), (int)controlViewSize.y));
	UpdateUI(controlViewSize, controlViewCenter);

	return true;
}

const bool GameGraphics::LoadUnitTextures()
{
	return UnitBuilder::LoadUnitTextures(_unitTextures);
}

const std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>& GameGraphics::GetUnitTextures() const
{
	return *_unitTextures;
}

void GameGraphics::UpdateUI(sf::Vector2f controlViewSize, sf::Vector2f controlViewCenter)
{
	_UIBackgroundSprite->setPosition(sf::Vector2f(
		controlViewCenter.x + controlViewSize.x / 2.f - controlViewSize.x / 6.f,
		controlViewCenter.y - controlViewSize.y / 2.f));
}

void GameGraphics::RenderGameWorld(sf::RenderWindow & renderWindow) const
{
	for (auto sprite = _gameWorldSprites.begin(); sprite != _gameWorldSprites.end(); sprite++)
	{
		auto& currentSprite = *sprite;
		renderWindow.draw(*currentSprite);
	}

	/*for (auto sprite = _unitSprites.begin(); sprite != _unitSprites.end(); sprite++)
	{
		renderWindow.draw(**sprite);
	}*/
}

void GameGraphics::RenderUI(sf::RenderWindow & renderWindow) const
{
	renderWindow.draw(*_UIBackgroundSprite);
}

void GameGraphics::AddUnitSprite(std::shared_ptr<sf::Sprite> newsprite)
{
	_unitSprites.push_back(newsprite);
}
