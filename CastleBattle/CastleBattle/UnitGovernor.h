#ifndef UGovernor
#define UGovernor

#include <vector>

#include "GameData.h"
#include "MapNavigator.h"

class UnitGovernor
{
public:
	static void Govern(const std::vector<std::unique_ptr<Unit>> & _units);
};

#endif // !UGovernor

