#ifndef GGraphics
#define GGraphics

#include <vector>
#include <memory>
#include <map>
#include <set>

#include <SFML/Graphics.hpp>

#include "UnitEnumTypes.h"
#include "UnitBuilder.h"

#define GameGraphicsPointer std::unique_ptr<GameGraphics>
#define RenderWindowPointer std::unique_ptr<sf::RenderWindow>
#define ControlViewPointer std::unique_ptr<sf::View>

const float WorldWidthInTiles = 32;
const float WorldHeightInTiles = 16;

class GameGraphics
{
public:
	GameGraphics();
	GameGraphics(sf::Vector2f t_tileSize) :
		m_tileSize(t_tileSize),
		m_gameWorldDimensions(sf::Vector2f(
			WorldWidthInTiles* t_tileSize.x,
			WorldHeightInTiles* t_tileSize.y))
	{}

	void SetGameWorldSizeByTile(sf::Vector2f t_newWorldTileSize);
	void SetGameWorldSizeByWorldDimensions(sf::Vector2f t_newWorldDimensions);

	const bool LoadGameGraphics();

	const bool LoadGameUI(sf::Vector2f controlViewSize, sf::Vector2f controlViewCenter);

	const std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>> & GetUnitTextures() const;

	void UpdateUI(sf::Vector2f controlViewSize, sf::Vector2f controlViewCenter);

	void RenderGameWorld(sf::RenderWindow & renderWindow) const;

	void RenderUI(sf::RenderWindow & renderWindow) const;

	const sf::Vector2f GetGameWorldSize() const { return m_gameWorldDimensions; }

	void AddUnitSprite(std::shared_ptr<sf::Sprite> newsprite);

private:
	sf::Vector2f m_tileSize;
	sf::Vector2f m_gameWorldDimensions;

	const bool LoadGameWorld();
	const bool LoadUnitTextures();

	std::vector<std::unique_ptr<sf::Texture>> _gameWorldTextures;
	std::vector<std::unique_ptr<sf::Sprite>> _gameWorldSprites;

	std::unique_ptr<sf::Texture> _UIBackgroundTexture;
	std::unique_ptr<sf::Sprite> _UIBackgroundSprite;

	std::unique_ptr<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>> _unitTextures;

	std::vector<std::shared_ptr<sf::Sprite>> _unitSprites;
};

#endif // !GGraphics

