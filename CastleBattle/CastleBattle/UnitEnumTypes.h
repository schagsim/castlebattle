#ifndef UEnumType
#define UEnumType

enum class UnitFaction { Neutral, Human };
enum class UnitType { Unspecified, HumanMarauder };
enum class ArmorType { None, Light, Medium, Heavy };
enum class DamageType { None, Splash, Normal, Pierce };

#endif // !UEnumType

