#include "PlayerController.h"

void PlayerController::AcceptInput() const
{
	sf::Event event;
	while (m_renderWindow->pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			m_renderWindow->close();

			break;
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left))
	{
		if ((m_controlView->getCenter().x - (m_controlView->getSize().x / 2.f)) > 0.f) m_controlView->move(sf::Vector2f(-2.f, 0));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right))
	{
		if ((m_controlView->getCenter().x + (m_controlView->getSize().x / 2.f)) < (m_gameController->GetGameWorldSize().x + (m_controlView->getSize().x / 6.f))) m_controlView->move(sf::Vector2f(2.f, 0));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down))
	{
		if ((m_controlView->getCenter().y + (m_controlView->getSize().y / 2.f)) < m_gameController->GetGameWorldSize().y) m_controlView->move(sf::Vector2f(0, 1.f));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up))
	{
		if ((m_controlView->getCenter().y - (m_controlView->getSize().y / 2.f)) > 0.f) m_controlView->move(sf::Vector2f(0, -1.f));
	}
	m_gameController->UpdateUI(m_controlView->getSize(), m_controlView->getCenter());
}

void PlayerController::ProcessActions()
{
	//m_gameController->SpawnNextWave(UnitFaction::Human, _nextWave, 2);

	if (_nextWaveTimer.getElapsedTime().asSeconds() > 30)
	{
		m_gameController->SpawnNextWave(UnitFaction::Human, _nextWave, 2);
		_nextWaveTimer.restart();
	}
}
