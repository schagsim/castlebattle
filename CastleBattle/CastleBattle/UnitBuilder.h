#ifndef UnitBuild
#define UnitBuild

#include "Unit.h"
#include "HumanUnitBuilder.h"

class UnitBuilder
{
public:
	static std::unique_ptr<Unit> CreateUnit(
		UnitFaction unitFaction, UnitType unitType,
		sf::Vector2f unitInitialPosition, sf::Vector2f unitTargetPosition,
		int playerNoAllegience,
		const std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>> & unitTextures);

	static bool LoadUnitTextures(std::unique_ptr<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>> & unitTextures);
};

#endif // !UnitBuild
