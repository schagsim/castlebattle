#ifndef GameControl
#define GameControl

#include "GameData.h"
#include "GameGraphics.h"

#define GameControllerPointer std::unique_ptr<GameController>

class GameController
{
public:
	GameController(const GameDataPointer & t_gameData, const GameGraphicsPointer & t_gameGraphics) :
		m_gameData(t_gameData), m_gameGraphics(t_gameGraphics) {}

	const sf::Vector2f GetGameWorldSize() const { return m_gameGraphics->GetGameWorldSize(); }

	void UpdateUI(sf::Vector2f controlViewSize, sf::Vector2f controlViewCenter) { m_gameGraphics->UpdateUI(controlViewSize, controlViewCenter); }

	void SpawnNextWave(const UnitFaction faction, const std::vector<UnitType> & nextWave, const int playerAllegiance);
private:
	const GameDataPointer & m_gameData;
	const GameGraphicsPointer & m_gameGraphics;
};

#endif // !GameControl
