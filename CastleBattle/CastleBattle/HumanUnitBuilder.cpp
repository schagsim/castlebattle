#include "HumanUnitBuilder.h"

std::unique_ptr<Unit> HumanUnitBuilder::CreateNewUnit(
	UnitType unitType,
	sf::Vector2f unitInitialPosition, sf::Vector2f unitTargetPosition,
	int playerNoAllegiance,
	const std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>> & unitTextures)
{
	std::unique_ptr<Unit> newUnit;

	sf::Texture tmpTexture;
	sf::Texture & unitTexture = tmpTexture;
	if (!(FindTextureInSet(unitType, unitTextures, unitTexture))) return nullptr;

	std::shared_ptr<sf::Sprite> unitSprite;

	switch (unitType)
	{
		case UnitType::HumanMarauder:
			unitSprite = std::make_shared<sf::Sprite>();
			unitSprite->setTexture(unitTexture);
			unitSprite->setTextureRect(sf::IntRect(0, 0, unitSprite->getTexture()->getSize().x, unitSprite->getTexture()->getSize().y));
			unitSprite->setOrigin(sf::Vector2f(unitSprite->getGlobalBounds().width / 2, unitSprite->getGlobalBounds().height / 2));
			unitSprite->setScale(sf::Vector2f(40.f / unitSprite->getTexture()->getSize().x, 40.f / unitSprite->getTexture()->getSize().y));
			unitSprite->setPosition(unitInitialPosition);
			unitSprite->setRotation(180.f + (atan((unitInitialPosition.y - unitTargetPosition.y) / (unitInitialPosition.x - unitTargetPosition.x)) * 180.f / 3.1415f));

			newUnit = std::make_unique<Unit>(
				100,
				playerNoAllegiance,
				unitInitialPosition, unitTargetPosition,
				unitSprite, nullptr,
				20.f,
				UnitFaction::Human, UnitType::HumanMarauder,
				ArmorType::Medium, DamageType::Normal,
				60.f);

			break;

		default:
			newUnit = nullptr;

			break;
	}

	return std::move(newUnit);
}

bool HumanUnitBuilder::LoadUnitTextures(std::unique_ptr<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>> & unitOutMap)
{
	std::unique_ptr<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>> units = std::make_unique<std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>>>();

	std::unique_ptr<sf::Texture> marauderTexture = std::make_unique<sf::Texture>();

	if (!(marauderTexture->loadFromFile("../../Resources/Textures/HumanTankMarauderMediumNormal.png"))) return false;

	units->insert(std::make_unique<std::pair<UnitType, std::unique_ptr<sf::Texture>>>(UnitType::HumanMarauder, std::move(marauderTexture)));

	unitOutMap = std::move(units);

	return true;
}

bool HumanUnitBuilder::FindTextureInSet(const UnitType unitType, const std::set<std::unique_ptr<std::pair<UnitType, std::unique_ptr<sf::Texture>>>> & unitTextures, sf::Texture & foundTexture)
{
	for (auto pair = unitTextures.begin(); pair != unitTextures.end(); pair++)
	{
		auto & currentPair = **pair;
		if (currentPair.first == unitType)
		{
			foundTexture = *currentPair.second;
			return true;
		}
	}
	return false;
}
