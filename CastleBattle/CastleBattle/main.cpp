#include <SFML/Graphics.hpp>

#include "CastleBattleGame.h"

int main()
{
	CastleBattleGame game;
	game.Start();

	return 0;
}