#include "PlayerGraphicController.h"

void PlayerGraphicController::Render() const
{
	m_renderWindow->clear();

	m_renderWindow->setView(*m_controlView);

	m_gameGraphics->RenderGameWorld(*m_renderWindow);
	m_gameGraphics->RenderUI(*m_renderWindow);

	m_renderWindow->display();
}
